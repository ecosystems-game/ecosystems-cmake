macro(project_add_library TARGET)
    cmake_parse_arguments(THIS "" "" "SOURCES;INCLUDES;COMPILE_FLAGS;DEPENDS;EXTERNAL_LIBS;LINK_DIRECTORIES" ${ARGN})
    string(STRIP ${TARGET} target)
    set(target "${target}")

    string(REPLACE "-" "_" NAME_UPPER ${target})
    string(TOUPPER "${NAME_UPPER}" NAME_UPPER)
    add_library(${target} ${THIS_SOURCES})

    set_target_properties(${target} PROPERTIES DEFINE_SYMBOL ${NAME_UPPER}_EXPORTS)
    set_target_properties(${target} PROPERTIES DEBUG_POSTFIX d)
    set_target_properties(${target} PROPERTIES RELEASE_POSTFIX "")
    set_target_properties(${target} PROPERTIES MINSIZEREL_POSTFIX "")
    set_target_properties(${target} PROPERTIES RELWITHDEBINFO_POSTFIX "")

    if (NOT ECOSYSTEMS_PLATFORM_ANDROID)
        set_target_properties(${target} PROPERTIES SOVERSION ${VERSION_MAJOR}.${VERSION_MINOR})
        set_target_properties(${target} PROPERTIES VERSION ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}.${VERSION_BUILD})
    endif()

    target_include_directories(${target} PUBLIC ${PROJECT_INCLUDE_ROOT})

    if (THIS_INCLUDES)
        target_include_directories(${target} PUBLIC ${THIS_INCLUDES})
    endif()

    if (CMAKE_BUILD_TYPE STREQUAL "Debug")
        target_compile_options(${target} PRIVATE -Wall)
    endif()

    if (PLATFORM_MAC)
        target_compile_options(${target} PUBLIC $<$<COMPILE_LANGUAGE:CXX>:-std=c++1z>)
    elseif (PLATFORM_WINDOWS)
        target_compile_options(${target} PUBLIC $<$<COMPILE_LANGUAGE:CXX>:/std:c++17>)
        target_compile_options(${target} PUBLIC $<$<COMPILE_LANGUAGE:CXX>:-fext-numeric-literals>)
    elseif (PLATFORM_LINUX)
        target_compile_options(${target} PUBLIC $<$<COMPILE_LANGUAGE:CXX>:-std=c++17>)
        target_compile_options(${target} PUBLIC $<$<COMPILE_LANGUAGE:CXX>:-fext-numeric-literals>)
        target_link_libraries(${target} -lpthread)
    endif()

    # Add specific compile flags
    if (THIS_COMPILE_FLAGS)
        target_compile_options(${target} PRIVATE ${THIS_COMPILE_FLAGS})
    endif()

    # Link Salem2D libraries
    if (THIS_DEPENDS)
        target_link_libraries(${target} ${THIS_DEPENDS})
    endif()

    # Link external libraries
    if (THIS_EXTERNAL_LIBS)
        target_link_libraries(${target} ${THIS_EXTERNAL_LIBS})
    endif()

    if(THIS_LINK_DIRECTORIES)
        target_link_directories(${target} ${THIS_LINK_DIRECTORIES})
    endif()

    # TODO: Add an install rule

endmacro()